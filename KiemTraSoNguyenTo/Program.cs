﻿
Console.Write("Nhap mot so: ");
int number = int.Parse(Console.ReadLine());
bool check = true;

for (int i = 2; i < number; i++)
{
    if(number % i == 0)
    {
        check = false; 
        break;
    }
}

if (check)
{
    Console.WriteLine($"{number} la so nguyen to.");
}
else
{
    Console.WriteLine($"{number} khong phai la so nguyen to.");
}