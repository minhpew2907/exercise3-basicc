﻿
Console.Write("Nhap mot so nguyen duong N: ");
int number = int.Parse(Console.ReadLine());
int firstNumb = 0;
int secondNumb = 1;
int next;

Console.Write($"\nDay Fibonacci voi {number} so la: ");
for (int i = 0; i < number; i++)
{
    if (i <= 1)
    {
        next = i;
    }
    else
    {
        next = firstNumb + secondNumb;
        firstNumb = secondNumb;
        secondNumb = next;
    }
    Console.Write(next + " ");
}
Console.WriteLine();